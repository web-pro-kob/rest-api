import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemperatureService } from './temperature.service';
import { TemperatureController } from './temperature.controller';
import { UsersModule } from './users/users.module';

@Module({
  imports: [UsersModule],
  controllers: [AppController, TemperatureController],
  providers: [AppService, TemperatureService],
  exports: [],
})
export class AppModule {}
