import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return 'hello buu';
  }
  @Post('world')
  getWorld(): string {
    return 'hello chanon';
  }

  @Get('test-query')
  testQuery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    return { celsius, type };
  }

  @Get('test-param/:celsius')
  testParam(@Req() req, @Param('celsius') celsius: number) {
    return { celsius };
  }

  @Post('test-body')
  testBody(@Req() req, @Body() body, @Body('celsius') celsius: number) {
    return { celsius };
  }

  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return this.appService.convert(parseFloat(celsius));
  }

  @Post('convert')
  convertByPost(@Body('celsius') celsius: number) {
    return this.appService.convert(celsius);
  }

  @Get('convert/:celsius')
  convertParam(@Param('celsius') celsius: string) {
    return this.appService.convert(parseFloat(celsius));
  }
}
